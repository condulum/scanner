`use strict`

console.log('Program Started.')

const Promise = require('bluebird'); //use bluebird instead of es6 implementation
const moment = require('moment'); //turn epoch into proper time
const events = require('events'); //event emitter

moment.locale('hk'); //change locale of moment to Hong Kong for proper timezone.

let eventEmitter = new events.EventEmitter(); //create new EventEmitter

//Web Logic
const express = require('express'); //webserver framework
const app = express(); //webapp

const server = require('http').Server(app); //http server
const io = require('socket.io')(server); //socket to send data to webpage

server.listen(8080); //start listening.

//minimal logic
app.get('/', (req,res) => {
  res.sendFile(__dirname + '/index.html');
});

const lib = require('pogobuf'); //pogobuf main library
const proto = require('node-pogo-protos'); //protobuf for pogo

let Trainer = new lib.PTCLogin(), //login to PTC
    client = new lib.Client(); //setup client

const config = require('./config.json');

const username = config.username, //username
      password = config.password;  //password

let loc = require('./loc.json').loc; //location json file
let numOfLocs = loc.length - 1,  //used in array manipulation
    current = numOfLocs;


function changeLoc(numOfLocs) {
  let lat = loc[numOfLocs]["lat"], 
      long = loc[numOfLocs]["long"]; //sets local variable for lat and long

  setTimeout(() => {
    client.setPosition(lat, long); //sets client location
    client.playerUpdate()  //update player location.
    console.log(`Current Location: ${lat}, ${long}`);

    let cellIDs = lib.Utils.getCellIDs(lat, long, 1); //get current cellID(s), array

    client.getMapObjects(cellIDs, Array(cellIDs.length).fill(0)).then(cellList => {
      let cell = cellList.map_cells[0]; //set cell variable as the first data in map_cells
      //console.log(cell); totally debug purpose.
      console.log(`Current cell #${cell.s2_cell_id.toString()} has ${cell.catchable_pokemons.length} catchable Pokemon(s).`);
      cell.catchable_pokemons.forEach(catchablePokemon => {
        let Pokemon = lib.Utils.getEnumKeyByValue(proto.Enums.PokemonId, catchablePokemon.pokemon_id),
            locLat = catchablePokemon.latitude,
            locLong = catchablePokemon.longitude,
            till = moment(catchablePokemon.expiration_timestamp_ms).format("HH:mm:ss");  //again, setup variables for easy manipulation.

        eventEmitter.emit('data', {
          Pokemon:Pokemon, 
          loc:`${locLat},${locLong}`, 
          till:till, 
          link: `http://maps.google.com/?q=@${locLat},${locLong}`});

        console.log(` - ${Pokemon} (${locLat}, ${locLong}) until ${till} .`);  //console output
        console.log(`http://maps.google.com/?q=@${locLat},${locLong}`);
      });
    });

    if (current > 0) {
      changeLoc(--current)
    } else if (current == 0) {
      changeLoc(current);
      process.exit(0);
    };
  }, 10*1000);
}

Trainer.login(username, password)
.then(token => {
  console.log('Setting Token.')
  client.setAuthInfo('ptc', token); //get token
  client.setPosition(loc[0].lat, loc[0].long) //set initial location
  return client.init();
}).then(() => {
  console.log('Initialized. (Authenticated.)');
  console.log('Start scanning.')
  changeLoc(numOfLocs);
}).catch(console.error);

io.on('connection', (socket) => {
  eventEmitter.on('data', function(data) {
    socket.emit('scanner', data);
  })
})