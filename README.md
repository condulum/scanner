# Pokemon Go Node.js Scanner - by Condulum

This scanner scans the Tseung Kwan O area in Hong Kong. More details (such as latitudes and longitudes can be found in the loc.json file.

Releases will soon be created. In the mean time, you can edit config.json and enter your PTC account in there. 

Initialize with `npm install`, it will install all the dependencies of the app.

Run the script with `node ./app.js`. Note that this app only runs once over the locations. You'd have to restart the app manually for another iteration.

Go to http://localhost:8080 for a prettier output.

(Automatic loop coming soon...?)

This app with forking capability (workers) is being actively developed.